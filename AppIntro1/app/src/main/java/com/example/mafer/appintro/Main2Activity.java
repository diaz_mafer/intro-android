package com.example.mafer.appintro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Objects;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent recibir =getIntent();
        String text = recibir.getStringExtra(MainActivity.EXTRA_MESSAGE);
        setContentView(R.layout.activity_main2);
        TextView planeta = findViewById(R.id.textView);
        planeta.setText(text);
    }
}
