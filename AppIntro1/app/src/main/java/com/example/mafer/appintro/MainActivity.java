package com.example.mafer.appintro;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    ListView listaplanets;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listaplanets = (ListView)findViewById(R.id.lv01);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.listado, android.R.layout.simple_list_item_1);
        listaplanets.setAdapter(adapter);

        listaplanets.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                            //Abre la activity 2
                            Intent test = new Intent(view.getContext(), Main2Activity.class);
                            int item = i;
                            String itemVal = (String)listaplanets.getItemAtPosition(i);
                            test.putExtra(EXTRA_MESSAGE, itemVal);
                            startActivity(test);

                    }
                }
        );

    }
}
